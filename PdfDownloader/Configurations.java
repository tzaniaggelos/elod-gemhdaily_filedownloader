package PdfDownloader;

public class Configurations {

	//public String filePathLocal = "C:/Users/Aggelos/workspace/GemhScrap/Pdf/";			// Local file path for pdf storing
	//public String csvFilePathLocal = "C:/Users/Aggelos/workspace/GemhScrap/Pdf/";		// Local file of csv with pdf links
	
	public String filePathLocal = "C:/Users/Aggelos/Desktop/test/";
	public String csvFilePathLocal = "C:/Users/Aggelos/Desktop/test/";
	
	public String filePathServer = "/home/tzanis/GemhPdfArchives/";			            // Server file path for pdf storing 
	public String csvFilePathServer = "/home/tzanis/GemhDaily/Pdf/";					// Server file path for pdf storing
	//public String csvFilePathServer = "/home/tzanis/GemhDailyAll/ex1/";					// Server file path for pdf storing
	public String csvExportServer = "/home/tzanis/GemhPdfArchives/Export/";				// Server file path for pdf storing
	
	public String ffPath = "/usr/bin/firefox";											// firefox path in the server
	public String displayNum = "10";													// the number of virtual display
			
	public boolean local = false;
}
