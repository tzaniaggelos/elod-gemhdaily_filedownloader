package PdfDownloader;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.comparator.LastModifiedFileComparator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;

import elod.tool.companyDb.CompanyDb;
import elod.tool.companyDb.GemhData;

public class HelperMethods {

	Configurations co = new Configurations();
	
	
	/** Enter sleep mode until 00:01:00 plus the value of int hours of the next day
	 * @param hour int hours how many hours after 00:01:00 we want to start again. The value must be between 0 - 23.
     * 
     * @throws InterruptedException
     */
    public static void sleepMode(int hour) throws InterruptedException{
    	
    	DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
    	Calendar cal = Calendar.getInstance();
    	String time = dateFormat.format(cal.getTime());
    	System.out.println(time);
    	    	
    	int Hour = Integer.parseInt(time.split(":")[0]);
		int min = Integer.parseInt(time.split(":")[1]);
		int sec = Integer.parseInt(time.split(":")[2]);
		
		int sleep = ((24 - Hour - 1 + hour)*3600)+((60-min)*60)+((60-sec)*1);
		
		int sleepHour = sleep/3600;
		int sleepMin = (sleep%3600)/60;
		int sleepSec = (sleep%3600)%60;
		
		System.out.println("Entering sleep mode for " +corectTimeFormat(sleepHour)+ ":" +corectTimeFormat(sleepMin)+ ":" + corectTimeFormat(sleepSec));
		
		TimeUnit.SECONDS.sleep(sleep);										// Sleep 10 seconds
	
	}
    
    
    /** Gives the corect time format, if given hour 1 convert it to 01 etc
     * 
     * @param input String input
     * @return corectInput String corectInput
     */
    public static String corectTimeFormat(int input){
    	
    	String corectInput = Integer.toString(input);
    	
    	if (corectInput.length() == 1){
    		corectInput = "0" +corectInput;
    	}
    	
		return corectInput ;
	
	}
	
	/**
     * Find the previus date into the yyyy-MM-dd format.
     * 
     * @return previusDate String the previus date in the yyyy-MM-dd format
     */
	public String getPreviusDate() {
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	    Calendar cal = Calendar.getInstance();
	    cal.add(Calendar.DATE,-1);    
	    String previusDate = dateFormat.format(cal.getTime());
		
		return previusDate;
	}
	
	
	
	/** Firefox profile builder 
	 * 
	 * @param downloadPath the download directory path
	 * @return profile the firefox profile preferences
	 * **/
    public static FirefoxProfile firefoxProfile(String downloadPath){
    	
    	FirefoxProfile profile = new FirefoxProfile();

    	 profile.setPreference("browser.download.folderList", 2);
         profile.setPreference("browser.download.dir", downloadPath); 							//this will download pdf inside downloadPath.
         profile.setPreference("plugin.disable_full_page_plugin_for_types", "application/pdf, application/msword,"
         						+ "application/vnd.openxmlformats-officedocument.wordprocessingml.document,"
         						+ "image/tif, image/x-tif, image/tiff, image/x-tiff, application/tif, application/x-tif,"
         						+ "application/tiff, application/x-tiff");
         profile.setPreference("browser.helperApps.neverAsk.saveToDisk","application/msword,application/pdfss,"
         						+ "application/vnd.openxmlformats-officedocument.wordprocessingml.document, "
         						+ "image/tif, image/x-tif, image/tiff, image/x-tiff, application/tif, application/x-tif,"
         						+ "application/tiff, application/x-tiff" );
         profile.setPreference("browser.download.manager.showWhenStarting", false);
         profile.setPreference("pdfjs.disabled", true);
         
		return profile;
    	
    }
     
    /** Start firefox in headless gui 
     * 
     * @param ffPath the firefox path on server
     * @param displayNum the display number for Xvfb
     * @return firefoxBinary the firefox binary
     * 
     * **/
    public static FirefoxBinary firefoxBinary(String ffPath, String displayNum){
    	
    	// Setup firefox binary to start in Xvfb        
        displayNum = ":" + displayNum;											// Stores the display number created in xcfb
    	String Xport = System.getProperty(						
                "lmportal.xvfb.id", displayNum);								// Get the xvfb id
        final File firefoxPath = new File(System.getProperty(
                "lmportal.deploy.firefox.path", ffPath));						// Get the firefox path in the system
        FirefoxBinary firefoxBinary = new FirefoxBinary(firefoxPath);			// Create a new instance of the firefox binary 
        firefoxBinary.setEnvironmentProperty("DISPLAY", Xport);					// Set the display number to the firefoxBinary
    	
        return firefoxBinary;
        
    }
    
    
    
    /** Get the download site name from the download link
     * 
     * @param fileLink the download link of the gemh archive
     * @return site the download site name
     */
    public String getDownloadSite (String fileLink){
		
    	String data[] = fileLink.split("\\/");
    	String site = data[2];
    	System.out.println("Site : " +site);
    	
    	return site;
    	
    }
    
    
   /** Download the file of the Link and save it to the saveDir
     * @param fileLink  the file download link
     * @param saveDir the save directory path
     * @param vatId the company vat id
     *  
     * @throws InterruptedException
     * @return downloaded true if file is downloaded and false if it is not
     **/
    public boolean  fileDownload (String fileLink , String saveDir, String vatId) throws InterruptedException, IOException{
        
    	boolean downloaded = false;
    	int timer = 0;														// Wait time
    	String ffPath = co.ffPath;											// firefox path in the server
    	String displayNum = co.displayNum;									// the number of virtual display
    	
    	FirefoxProfile profile = firefoxProfile(saveDir);					// Creates a new profile
    	WebDriver driver = null;
    	
    	if (co.local){
    		driver = new FirefoxDriver(profile);							// Create a new instance of the firefox driver on local mode
    	}
    	else{
    		FirefoxBinary binary = firefoxBinary(ffPath, displayNum);			// Creates a new binary
        	driver = new FirefoxDriver(binary, profile);					// Create a new instance of the firefox driver on server mode
    	}
    	
    	while (!downloaded && timer < 61) {
    		
	    	driver.get(fileLink);												// Conects to the WebSite
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
  	
			downloaded = isFileDownloaded(saveDir, vatId);					
				
			if (!downloaded){													// wait until file is downloaded
				
				TimeUnit.SECONDS.sleep(2);
				timer = timer +2;												// increase the timer by two seconds
				
			}
	    	
		}
    	
    	driver.close();
		driver.quit();
		
		return downloaded;
		
	}
  	
    
    
    /** Get the newest file for a specific extension 
     * 
     * @param filePath the filepath of the download directory
     * 
     * @return theNewestFile the newest file in the filePath
     * @throws InterruptedException 
     * 
     * **/
    public File getTheNewestFile(String filePath) throws InterruptedException {
         
    	File theNewestFile = null;
    	
    	TimeUnit.SECONDS.sleep(2);
    	File dir = new File(filePath);									// Get the directory we want to scan
	    File[] files = dir.listFiles();									// Get the list of files in the directory
	
	    if (files.length > 0) {											// Check if there are files in the directory
	        	
		    // The newest file comes first after the sorting
		    Arrays.sort(files, LastModifiedFileComparator.LASTMODIFIED_REVERSE);
		    theNewestFile = files[0];									// Get the newest file
		    
		    if (files.length>1 && files[1].getName().contains("part")){
	        	
		    	files[0].delete();
		      	System.out.println("File is not yet completely downloaded!");
		       	TimeUnit.SECONDS.sleep(2);
		       	try {
					fileRenamer(files[1], files[1].getName().split("\\.")[0] +"."+ files[1].getName().split("\\.")[1] , filePath);
				} catch (IOException e) {
					e.printStackTrace();
				}
		       	Arrays.sort(files, LastModifiedFileComparator.LASTMODIFIED_REVERSE);
			    theNewestFile = files[0];
		    
		    }
		    else {
		       	System.out.println("File completely downloaded!");
		    }
	    }
	    return theNewestFile;
    }
    
    
    
    /** Check if the wanted file name exists in the directory 
     * 
     * @param filePath the filepath of the download directory
     * @param file the name of the file we want to search
     * @param fileNameGr 
     * 
     * @return found true if file already exists and false if not
     * @throws SQLException 
     * 
     * **/
    public static boolean fileExists(String filePath, String fileName, String fileNameGr) throws SQLException {
       
    	boolean found = false;
    	
        /*File dir = new File(filePath);									// Get the directory we want to scan
        File[] files = dir.listFiles();									// Get the list of files in the directory

        if (files.length > 0) {											// Check if there are files in the directory
        	
        	for (int i=0; i<files.length; i++){
        		
        		String fileName = files[i].getName().split("\\.")[0];
        		
        		if (file.equalsIgnoreCase(fileName)){
        			found = true;
        			break;
        		}
        		
        	}
            
        }*/
    	
    	/** CompanyDb Connection **/
		CompanyDb cdb=new CompanyDb("jdbc:mysql://83.212.86.155/DbCompany?useUnicode=true&characterEncoding=UTF-8"
									,"gemh"
									,"gemh@69");
    	
		List<String> detailsList = getDetails(filePath);
		List<GemhData> gd = null;
		
		// check with the greeklsish name
		gd=cdb.getPath(detailsList.get(0), detailsList.get(1), detailsList.get(2), detailsList.get(3), fileName, null); 

    	if(gd.size()>0){
    		found = true;
    	}
    	
    	// check with the greek name
    	if(!found && fileNameGr != null){
    		
    		gd=cdb.getPath(detailsList.get(0), detailsList.get(1), detailsList.get(2), detailsList.get(3), fileNameGr, null); 

        	if(gd.size()>0){
        		found = true;
        	}
        	
    	}

    	cdb.close();
    	
        return found;
        
    }
    
    
    
	/**
     * 
     * @param filePath
     * @return
     * @throws SQLException
     */
    public static List<String> getDetails(String filePath) throws SQLException {
        
    	
    	String orgType = null;
		String vatId = null;
		String gemhNumber = null;
		String category = null;
		
		
    	int size = filePath.split("\\/").length;
		
		if (size == 5){
			orgType = filePath.split("\\/")[1];
			vatId = filePath.split("\\/")[2];
			gemhNumber = filePath.split("\\/")[3];
			category = filePath.split("\\/")[4];
		}
		else if(size == 6){
			orgType = filePath.split("\\/")[2];
			vatId = filePath.split("\\/")[3];
			gemhNumber = filePath.split("\\/")[4];
			category = filePath.split("\\/")[5];
		}
		
		List<String> detailsList = Arrays.asList(orgType, vatId, gemhNumber, category);
		
		return detailsList;
    }
    
    
    
    /** Rename the last file of a directory
     * 
     *  @param oldFile File the file we want to rename
     *  @param newFileName String the name we want the file to get
     *  @param directory String the directory path were we want to save the file
     *  
     *  **/
    public void  fileRenamer (File oldFile , String newFileName, String directory) throws IOException{
    	
    	/* File (or directory) with new name */
		File newFile = new File(directory +"/"+ newFileName);
		
		if (newFile.exists()) {
			try {
				throw new java.io.IOException("File already exists!");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		/* Rename file */
		boolean success = oldFile.renameTo(newFile);
		
		String filePath = oldFile.getPath().replaceAll("\\(1\\)", "");
		File file = new File(filePath);
		
		if (!success) {
			System.out.println("Couldn't rename file!");
		} else {
			System.out.println("File renamed successfully!");
		}

		if (oldFile.exists()){
			if (oldFile.delete()) {
				System.out.println(oldFile.getName() + " is deleted!");
			} 
			else {
				System.out.println(oldFile.getName()+" Delete operation failed.");
			}
		}
		else if (file.exists()){
			if (file.delete()) {
				System.out.println(file.getName() + " is deleted!");
			} 
			else {
				System.out.println(file.getName()+" Delete operation is failed.");
			}
		}
		
	 }
    
    
    
    /** Create a new directory at directoryPath with the directoryName we want 
     * 
     * @param directoryPath the path of the directory we want to create
     * @param directoryName the name we want the directory to have
     * 
     * @return newDirectory the new directory
     * **/
    public File directoryCreator (String directoryPath, String directoryName) {
       
    	File newDirectory = new File(directoryPath, directoryName);		// Create a new directory
		
    	if(!newDirectory.exists()){										// Check if directory exists
			newDirectory.mkdirs();
		}
		
        return newDirectory;
    }
  	
    
    
    /** check if file has been downloaded 
     * 
     * @param downloadPath the directory path
     * @param vatId the vatId of the company. Every renamed doawnloaded archive,
     * contains in the archive name the company vatId
     * 
     * @return flag true if file is downloaded and false if not
     * **/
    public static boolean isFileDownloaded(String downloadPath, String vatId) {
    	
		boolean flag = false;
		
	    File dir = new File(downloadPath);								// Get the directory we want to scan
	    File[] dir_contents = dir.listFiles();							// Get all the files of directory
	  	    
	    for (int i = 0; i < dir_contents.length; i++) {
	    	
	        if (!dir_contents[i].getName().contains(vatId)){		// Check if the fie we want is in the directory
		            return flag=true;
		    } 
	    }

	    return flag;	
	    
	}
    
    
    
    
   /**
     * Export to a file the not downloaded pdf files
     * 
     * @param String fileName the output filename
     * @param String metadata the not downloaded lines of the pdf csv
     */
	public void writeMetadata(String fileName, String metadata) {
		
		Configurations co = new Configurations();
		
		try {
		    PrintWriter out = null;
		    if (co.local){
		    	out =new PrintWriter(new BufferedWriter(new FileWriter(fileName + ".csv", true)));
		    }
		    else{
		    	out =new PrintWriter(new BufferedWriter(new FileWriter(fileName + ".csv", true)));
		    }
		    out.println(metadata);
		    out.close();
		} catch (IOException e) {
		    e.printStackTrace();
		}
		
	}
	
	
	/** Get the type of the downloaded archive
	 * 
	 * @param file File the newest downloaded file
	 * @return fileType String the file ext type
	 */
	public String getFileType(File file) {

		String fileType = null;
		String[] data = file.getName().split("\\.");
		fileType = data[data.length-1];
		
		return fileType;
	}


	/** Convert a word with greek characters to the same word with greeklish characters.
	 * 
	 * @param greek String the word with greek characters.
	 * @return greeklish String the word with greeklish characters.
	 */
	public static String convertToGreeklish(String greek) {

		greek = greek.toUpperCase();				// convert to upper case
		greek = cleanSpecialChar(greek);			// replace the special characters
		
		greek = greek.replaceAll("�", "A");
		greek = greek.replaceAll("�", "B");
		greek = greek.replaceAll("�", "G");
		greek = greek.replaceAll("�", "D");
		greek = greek.replaceAll("�", "E");
		greek = greek.replaceAll("�", "Z");
		greek = greek.replaceAll("�", "I");
		greek = greek.replaceAll("�", "TH");
		greek = greek.replaceAll("�", "I");
		greek = greek.replaceAll("�", "K");
		greek = greek.replaceAll("�", "L");
		greek = greek.replaceAll("�", "M");
		greek = greek.replaceAll("�", "N");
		greek = greek.replaceAll("�", "X");
		greek = greek.replaceAll("�", "O");
		greek = greek.replaceAll("�", "P");
		greek = greek.replaceAll("�", "R");
		greek = greek.replaceAll("�", "S");
		greek = greek.replaceAll("�", "T");
		greek = greek.replaceAll("�", "Y");
		greek = greek.replaceAll("�", "F");
		greek = greek.replaceAll("�", "X");
		greek = greek.replaceAll("�", "PS");
		greek = greek.replaceAll("�", "O");
		
		return greek;
	}
	
	
	/**Clean the given string from the special characters e.g. 'A --> A,...
	 * 
	 * @param word String the string we want to clear.
	 * @return word String the clear from special characters given string.
	 */
	public static String cleanSpecialChar(String word) {

		word = word.replaceAll("�", "�");
		word = word.replaceAll("�", "�");
		word = word.replaceAll("�", "�");
		word = word.replaceAll("�", "�");
		word = word.replaceAll("�", "�");
		word = word.replaceAll("�", "�");
		word = word.replaceAll("�", "�");
		word = word.replaceAll("�", "�");
		word = word.replaceAll("�", "�");
		
		return word;
	}


}