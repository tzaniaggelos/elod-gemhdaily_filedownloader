package PdfDownloader;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.List;

import com.dropbox.core.DbxException;

import au.com.bytecode.opencsv.CSVReader;
import elod.tool.companyDb.CompanyDb;
import elod.tool.companyDb.GemhData;
import elod.tools.dropbox.DropboxApi;

/**
 * @author A. Tzanis
 */


/** 
 * file downloading code. Download the file from
 * the given link and store them to a specific
 * directory with specific file name 
 *  **/

@SuppressWarnings("unused")
public class FileDownloader {

	@SuppressWarnings({ "static-access"})
	public static void main(String[] args) throws IOException, InterruptedException, SQLException {
		
		Configurations co = new Configurations();
		HelperMethods hm = new HelperMethods();
		
		DropboxApi dba = new DropboxApi();

		/** CompanyDb Connection **/
    	/*CompanyDb cdb=new CompanyDb("jdbc:mysql://83.212.86.155/DbCompany?useUnicode=true&characterEncoding=UTF-8"
									,"gemh"
									,"gemh@69");
    	
		List<GemhData> gd=cdb.getPath(null,null,null,null,"������ ���_2016-12-27_139452103",null); 
    	for(GemhData g:gd){ 
    	       System.out.println(g.getRelativePath());
    	       System.out.println(g.getDropboxUrl());
    	       hm.writeMetadata("test1_" +hm.getPreviusDate(), g.getRelativePath());
				
    	}*/
    	
		String downloadPath = null;
		String filePath = null;
		String csvFilePath = null;
		String fileLink = null;
		String[] fields;
		boolean run = true;
		
		String data = null;
		
		if (co.local){
			filePath = co.filePathLocal;					// Local file storing path
			csvFilePath = co.csvFilePathLocal;				// Local csv path
		}
		else{
			filePath = co.filePathServer;					// Server file storing path
			csvFilePath = co.csvFilePathServer;				// Server csv path
		}
		
		/** FOLDER CREATION **/
		File ExportDirectory = hm.directoryCreator (filePath, "Export");					// Create Export directory
		File AeDirectory = hm.directoryCreator (filePath, "PLC");							// Create PLC directory
		File AebeDirectory = hm.directoryCreator (filePath, "PLC/PLC.AEBE");				// Create PLC/PLC.AEBE directory
		File AbeeDirectory = hm.directoryCreator (filePath, "PLC/PLC.ABEE");				// Create PLC/PLC.ABEE directory
		File AbeteDirectory = hm.directoryCreator (filePath, "PLC/PLC.ABETE");				// Create PLC/PLC.ABETE directory
		File AteeDirectory = hm.directoryCreator (filePath, "PLC/PLC.ATEE");				// Create PLC/PLC.ATEE directory
		File AteDirectory = hm.directoryCreator (filePath, "PLC/PLC.ATE");					// Create PLC/PLC.ATE directory
		File EpeDirectory = hm.directoryCreator (filePath, "LTD");							// Create LTD directory
		File MonEpeDirectory = hm.directoryCreator (filePath, "LTD/LTD.SM");				// Create LTD/LTD.SM directory
		File MonAeDirectory = hm.directoryCreator (filePath, "PLC/PLC.SM");					// Create PLC/PLC.SM directory
		File EeDirectory = hm.directoryCreator (filePath, "LP");							// Create LP directory
		File OeDirectory = hm.directoryCreator (filePath, "GP");							// Create GP directory
		File IkeDirectory = hm.directoryCreator (filePath, "PC");							// Create PC directory
		File MonIkeDirectory = hm.directoryCreator (filePath, "PC/PC.SM");					// Create PC/PC.SM directory
		File FrDirectory = hm.directoryCreator (filePath, "FR");							// Create FR directory
		File AmkeDirectory = hm.directoryCreator (filePath, "NPO");							// Create NPO directory
		File CoopDirectory = hm.directoryCreator (filePath, "COOP");						// Create COOP directory
		File CoopAgriDirectory = hm.directoryCreator (filePath, "COOP/COOP.AGRI");			// Create COOP/COOP.AGRI directory
		File JvDirectory = hm.directoryCreator (filePath, "JV");							// Create JV directory
		File PlcSpecialDirectory = hm.directoryCreator (filePath, "PLC/PLC.SPECIAL");		// Create PLC/PLC.SPECIAL directory
		File LtdSpecialDirectory = hm.directoryCreator (filePath, "LTD/LTD.SPECIAL");		// Create LTD/LTD.SPECIAL directory
		File ForeignDirectory = hm.directoryCreator (filePath, "FOREIGN");					// Create FOREIGN directory
		File PublicDirectory = hm.directoryCreator (filePath, "PUBLIC");					// Create PUBLIC directory
		File PublicPleDirectory = hm.directoryCreator (filePath, "PUBLIC/PUBLIC.PLE");		// Create PUBLIC/PUBLIC.PLE directory
		File PublicMunicDirectory = hm.directoryCreator (filePath, "PUBLIC/PUBLIC.MUNIC");	// Create PUBLIC/PUBLIC.MUNIC directory
		File LawersDirectory = hm.directoryCreator (filePath, "LAWERS");					// Create LAWERS directory
		File NotariesDirectory = hm.directoryCreator (filePath, "NOTARIES");				// Create NOTARIES directory
		File BailiffDirectory = hm.directoryCreator (filePath, "BAILIFF");					// Create BAILIFF directory
		File AssociationDirectory = hm.directoryCreator (filePath, "ASSOCIATION");			// Create ASSOCIATION directory
		File ShipDirectory = hm.directoryCreator (filePath, "SHIP");						// Create SHIP directory
		File FoundationDirectory = hm.directoryCreator (filePath, "FOUNDATION");			// Create FOUNDATION directory
		File InternationalDirectory = hm.directoryCreator (filePath, "INTERNATIONAL");		// Create INTERNATIONAL directory
		File CharityDirectory = hm.directoryCreator (filePath, "CHARITY");					// Create CHARITY directory
		File PoliticalDirectory = hm.directoryCreator (filePath, "POLITICAL");				// Create POLITICAL directory
		File UnDirectory = hm.directoryCreator (filePath, "OTHER");							// Create OTHER directory
		
		/** PATH CREATION **/
		String ExportPath = ExportDirectory.getPath().toString();						// Get Export directory path
		String AePath = AeDirectory.getPath().toString();								// Get PLC directory path
		String AebePath = AebeDirectory.getPath().toString();							// Get PLC/AEBE directory path
		String AbeePath = AbeeDirectory.getPath().toString();							// Get PLC/ABEE directory path
		String AbetePath = AbeteDirectory.getPath().toString();							// Get PLC/ABETE directory path
		String AteePath = AteeDirectory.getPath().toString();							// Get PLC/ATEE directory path
		String AtePath = AteDirectory.getPath().toString();								// Get PLC/ATE directory path
		String EpePath = EpeDirectory.getPath().toString();								// Get LTD directory path
		String MonEpePath = MonEpeDirectory.getPath().toString();						// Get LTD/LTD.SM directory path
		String MonAePath = MonAeDirectory.getPath().toString();							// Get PLC/PLC.SM directory path
		String EePath = EeDirectory.getPath().toString();								// Get LP directory path
		String OePath = OeDirectory.getPath().toString();								// Get GP directory path
		String IkePath = IkeDirectory.getPath().toString();								// Get PC directory path
		String MonIkePath = MonIkeDirectory.getPath().toString();						// Get PC/PC.SM directory path
		String FrPath = FrDirectory.getPath().toString();								// Get FR directory path
		String AmkePath = AmkeDirectory.getPath().toString();							// Get NPO directory path
		String CoopPath = CoopDirectory.getPath().toString();							// Get COOP directory path
		String CoopAgriPath = CoopAgriDirectory.getPath().toString();					// Get COOP/COOP.AGRI directory path
		String JvPath = JvDirectory.getPath().toString();								// Get JV directory path
		String PlcSpecialPath = PlcSpecialDirectory.getPath().toString();				// Get PLC/PLC.SPECIAL directory path
		String LtdSpecialPath = LtdSpecialDirectory.getPath().toString();				// Get LTD/LTD.SPECIAL directory path
		String ForeignPath = ForeignDirectory.getPath().toString();						// Get FOREIGN directory path
		String PublicPath = PublicDirectory.getPath().toString();						// Get PUBLIC directory path
		String PublicPlePath = PublicPleDirectory.getPath().toString();					// Get PUBLIC/PUBLIC.PLE directory path
		String PublicMunicPath = PublicMunicDirectory.getPath().toString();				// Get PUBLIC/PUBLIC.PLE directory path
		String LawersPath = LawersDirectory.getPath().toString();						// Get LAWERS directory path
		String NotariesPath = NotariesDirectory.getPath().toString();					// Get NOTARIES directory path
		String BailiffPath = BailiffDirectory.getPath().toString();						// Get BAILIFF directory path
		String AssociationPath = AssociationDirectory.getPath().toString();				// Get ASSOCIATION directory path
		String ShipPath = ShipDirectory.getPath().toString();							// Get SHIP directory path
		String FoundationPath = FoundationDirectory.getPath().toString();				// Get FOUNDATION directory path
		String InternationalPath = InternationalDirectory.getPath().toString();			// Get INTERNATIONAL directory path
		String CharityPath = CharityDirectory.getPath().toString();						// Get CHARITY directory path
		String PoliticalPath = PoliticalDirectory.getPath().toString();					// Get POLITICAL directory path
		String UnPath = UnDirectory.getPath().toString();								// Get OTHER directory path
		
		while (run){
			
			hm.sleepMode(2);															// Enter in sleep mode until next next day at 02:01:00
        	
			String csvFile = csvFilePath + "Pdf_" +hm.getPreviusDate()+ ".csv";			// Get the previus date, to find the csv we want to open
			
			try {
				
				@SuppressWarnings("resource")			
				CSVReader reader = new CSVReader(new InputStreamReader(new FileInputStream(csvFile), "UTF-8"), 
						 '~', '"', '\0');
				
				String[] nextLine;
	
				
				while ((nextLine = reader.readNext()) != null) {					// Reads the csv file by row 
					
					System.out.print("-----------------------------------------------------------");	
					
					fields = new String[nextLine.length];																			
						
					for (int i = 0; i < nextLine.length; i++) {						// fields[0]=afm, fields[1]=orgCategory, fields[2]=fekType,
					
						fields[i] = nextLine[i];									// fields[3]=fekThema, fields[4]=originalDate, fields[5]=fekNumber, fields[6]=fileLink
					
						System.out.print(fields[i]+"\t");	
						
						/** store nextLine to String data **/
						if (i == 0){
							data = fields[i];
						}
						else{
							data += "," + fields[i];
						}
					}
					
					System.out.print("\n");
					
					String vatId = fields[0].replaceAll("[\"]","");
					String gemh = fields[1].replaceAll("[\"]","");
					String orgType = fields[2].replaceAll("[\"]","");
					String gemhType = fields[3].replaceAll("[\"]","");
					
					String thisPath = "/" + vatId + "/" + gemh + "/" + gemhType;						// /AFM/GEMH/GEMH_TYPE
					File thisDirectory = null;										
				
						
					// Check if company is PLC
					if (orgType.equalsIgnoreCase("AE") || orgType.equalsIgnoreCase("��") 
							|| (fields[2].equalsIgnoreCase("PLC"))){	
							
						thisDirectory = hm.directoryCreator (AePath, thisPath);							// Create the exact directory of each gemhfile
						thisPath = "/PLC" + thisPath;													// /PLC/AFM/GEMH/GEMH_TYPE
					}
					// Check if company is PLC/AEBE
					else if (orgType.equalsIgnoreCase("PLC/AEBE")){	
							
						thisDirectory = hm.directoryCreator (AebePath, thisPath);						// Create the exact directory of each gemhfile
						thisPath = "/PLC/PLC.AEBE" + thisPath;											// /PLC/PLC.AEBE/AFM/GEMH/GEMH_TYPE
					}
					// Check if company is PLC/ABEE
					else if (orgType.equalsIgnoreCase("PLC/ABEE")){	
							
						thisDirectory = hm.directoryCreator (AbeePath, thisPath);						// Create the exact directory of each gemhfile
						thisPath = "/PLC/PLC.ABEE" + thisPath;											// /PLC/PLC.ABEE/AFM/GEMH/GEMH_TYPE
					}
					// Check if company is PLC/ABETE
					else if (orgType.equalsIgnoreCase("PLC/ABETE")){	
							
						thisDirectory = hm.directoryCreator (AbetePath, thisPath);						// Create the exact directory of each gemhfile
						thisPath = "/PLC/PLC.ABETE" + thisPath;											// /PLC/PLC.ABETE/AFM/GEMH/GEMH_TYPE
					}
					// Check if company is PLC/ATEE
					else if (orgType.equalsIgnoreCase("PLC/ATEE")){	
							
						thisDirectory = hm.directoryCreator (AteePath, thisPath);						// Create the exact directory of each gemhfile
						thisPath = "/PLC/PLC.ATEE" + thisPath;											// /PLC/PLC.ATEE/AFM/GEMH/GEMH_TYPE
					}
					// Check if company is PLC/ATE
					else if (orgType.equalsIgnoreCase("PLC/ATE")){	
							
						thisDirectory = hm.directoryCreator (AtePath, thisPath);						// Create the exact directory of each gemhfile
						thisPath = "/PLC/PLC.ATE" + thisPath;											// /PLC/PLC.ATE/AFM/GEMH/GEMH_TYPE
					}
					// Check if company is LTD
					else if (orgType.equalsIgnoreCase("���") || orgType.equalsIgnoreCase("LTD")){		
							
						thisDirectory = hm.directoryCreator (EpePath, thisPath);						// Create the exact directory of each Gemh File
						thisPath = "/LTD" + thisPath;													// /LTD/AFM/GEMH/GEMH_TYPE	
					}
					// Check if company is LTD.SM
					else if (orgType.equalsIgnoreCase("��� �����������") || orgType.equalsIgnoreCase("LTD/SM")){		
							
						thisDirectory = hm.directoryCreator (MonEpePath, thisPath);						// Create the exact directory of each Gemh File
						thisPath = "/LTD/LTD.SM" + thisPath;											// /LTD/LTD.SM/AFM/GEMH/GEMH_TYPE	
					}
					// Check if company is LP
					else if (orgType.equalsIgnoreCase("��") || orgType.equalsIgnoreCase("��") || orgType.equalsIgnoreCase("LP")){		
							
						thisDirectory = hm.directoryCreator (EePath, thisPath);				  	 		// Create the exact directory of each Gemh File
						thisPath = "/LP" + thisPath;											 		// /LP/AFM/GEMH/GEMH_TYPE	
					}
					// Check if company is GP
					else if (orgType.equalsIgnoreCase("OE") || orgType.equalsIgnoreCase("��") || orgType.equalsIgnoreCase("GP")){		
							
						thisDirectory = hm.directoryCreator (OePath, thisPath);				  			// Create the exact directory of each Gemh File
						thisPath = "/GP" + thisPath;													// /GP/AFM/GEMH/GEMH_TYPE	
					}
					// Check if company is PC
					else if (orgType.equalsIgnoreCase("�������� ������������� ��������") || orgType.equalsIgnoreCase("PC")){		
							
						thisDirectory = hm.directoryCreator (IkePath, thisPath);						// Create the exact directory of each Gemh File
						thisPath = "/PC" + thisPath;													// /PC/AFM/GEMH/GEMH_TYPE	
					}
					// Check if company is PC.SM
					else if (orgType.equalsIgnoreCase("�������� �������� ������������� ��������") || orgType.equalsIgnoreCase("����������� �������� ������������� ��������")
							|| orgType.equalsIgnoreCase("PC/SM")){		
							
						thisDirectory = hm.directoryCreator (MonIkePath, thisPath);						// Create the exact directory of each Gemh File
						thisPath = "/PC/PC.SM" + thisPath;												// /PC/PC.SM/AFM/GEMH/GEMH_TYPE	
					}
					// Check if company is NPO
					else if (orgType.equalsIgnoreCase("NPO") || orgType.equalsIgnoreCase("������ �� ������������ �������")
							||orgType.equalsIgnoreCase("�� ����������� ��������")){		
							
						thisDirectory = hm.directoryCreator (AmkePath, thisPath);				   		// Create the exact directory of each Gemh File
						thisPath = "/NPO" + thisPath;											   		// /NPO/AFM/GEMH/GEMH_TYPE	
					}
					// Check if company is COOP
					else if (orgType.equalsIgnoreCase("COOP") || orgType.equalsIgnoreCase("�������������") || orgType.equalsIgnoreCase("������� ������������� ���������.�������") 
							|| orgType.equalsIgnoreCase("������� ������������� ����������.�������")	|| orgType.equalsIgnoreCase("������� ������������� �������������") 
							|| orgType.equalsIgnoreCase("������� ������������� �� �������������") || orgType.equalsIgnoreCase("������� ������������� ������������� �������")
							|| orgType.equalsIgnoreCase("����� ������� �������������") || orgType.equalsIgnoreCase("���������� ������� �������������")
							|| orgType.equalsIgnoreCase("���������� ������������� ��� �������") || orgType.equalsIgnoreCase("������� ������������� ������������ �������") 
							|| orgType.equalsIgnoreCase("���������� ������������� ������������� �������")){		
							
						thisDirectory = hm.directoryCreator (CoopPath, thisPath);				   		// Create the exact directory of each Gemh File
						thisPath = "/COOP" + thisPath;											   		// /COOP/AFM/GEMH/GEMH_TYPE	
					}
					// Check if company is COOP.AGRI
					else if (orgType.equalsIgnoreCase("��������� �������������") || orgType.equalsIgnoreCase("����� ��������� ������������� �.�.�.")
							|| orgType.equalsIgnoreCase("�/�") || orgType.equalsIgnoreCase("���") || orgType.equalsIgnoreCase("�������.��������� ��������������� ����������")
							|| orgType.equalsIgnoreCase("�������.��������� ��������. ����������")){		
							
						thisDirectory = hm.directoryCreator (CoopAgriPath, thisPath);					// Create the exact directory of each Gemh File
						thisPath = "/COOP/COOP.AGRI" + thisPath;										// /COOP/COOP.AGRI/AFM/GEMH/GEMH_TYPE	
					}
					// Check if company is JV
					else if (orgType.equalsIgnoreCase("JV") || orgType.equalsIgnoreCase("�����������")){		
							
						thisDirectory = hm.directoryCreator (JvPath, thisPath);				   	   		// Create the exact directory of each Gemh File
						thisPath = "/JV" + thisPath;											   		// /JV/AFM/GEMH/GEMH_TYPE	
					}
					// Check if FR
					else if (orgType.equalsIgnoreCase("FR")){
							
						thisDirectory = hm.directoryCreator (FrPath, thisPath);					   		// Create the exact directory of each Gemh File
						thisPath = "/FR" + thisPath;												   	// /FR/AFM/GEMH/GEMH_TYPE	
					}
					// Check if PLC.SM
					else if (orgType.equalsIgnoreCase("PLC/SM") || orgType.equalsIgnoreCase("�� �����������")){
							
						thisDirectory = hm.directoryCreator (MonAePath, thisPath);				   		// Create the exact directory of each Gemh File
						thisPath = "/PLC/PLC.SM" + thisPath;										   	// /PLC/PLC.SM/AFM/GEMH/GEMH_TYPE	
					}
					// Check if PLC.SPECIAL
					else if (orgType.equalsIgnoreCase("�� �� ������������") || orgType.equalsIgnoreCase("������� �������� �� ������������")
							|| orgType.equalsIgnoreCase("�� ���� ������� ��� (�.2860/2000)") || orgType.equalsIgnoreCase("�� ���� (�.2956/2001)")
							|| orgType.equalsIgnoreCase("�������� �� �� �� ��") || orgType.equalsIgnoreCase("�������� �� �� ��")
							|| orgType.equalsIgnoreCase("������� �������� �� ������������")){
							
						thisDirectory = hm.directoryCreator (PlcSpecialPath, thisPath);				   	// Create the exact directory of each Gemh File
						thisPath = "/PLC/PLC.SPECIAL" + thisPath;										// /PLC/PLC.SPECIAL/AFM/GEMH/GEMH_TYPE	
					}
					// Check if LTD.SPECIAL
					else if (orgType.equalsIgnoreCase("�������� �� �� ���") || orgType.equalsIgnoreCase("�������� �� �� �� ���")
							|| orgType.equalsIgnoreCase("��� ��� �����������") || orgType.equalsIgnoreCase("��� ���")){
							
						thisDirectory = hm.directoryCreator (LtdSpecialPath, thisPath);				   	// Create the exact directory of each Gemh File
						thisPath = "/LTD/LTD.SPECIAL" + thisPath;										// /LTD/LTD.SPECIAL/AFM/GEMH/GEMH_TYPE	
					}
					// Check if FOREIGN
					else if (orgType.equalsIgnoreCase("������������ ��������� ��������") || orgType.equalsIgnoreCase("��������� �������� ���� �� ��")
							|| orgType.equalsIgnoreCase("������� �.�. 89/67") || orgType.equalsIgnoreCase("�������� ������� ����������� ��������")
							|| orgType.equalsIgnoreCase("�����.������� �� ����.����� ���� ������") || orgType.equalsIgnoreCase("AN 89/67 ��������� ��������")
							|| orgType.equalsIgnoreCase("�����.�����.��������� � ���������� �����") || orgType.equalsIgnoreCase("AN 89/67 ������� ��")
							|| orgType.equalsIgnoreCase("�� 89/67 ������� ���")){
							
						thisDirectory = hm.directoryCreator (ForeignPath, thisPath);				   	// Create the exact directory of each Gemh File
						thisPath = "/FOREIGN" + thisPath;										   		// /FOREIGN/AFM/GEMH/GEMH_TYPE	
					}
					// Check if company is PUBLIC
					else if (orgType.equalsIgnoreCase("������� ��������") || orgType.equalsIgnoreCase("�������� ���������� ����������") || orgType.equalsIgnoreCase("������ �������� ����������")
							|| orgType.equalsIgnoreCase("�������� �������������� ����������") || orgType.equalsIgnoreCase("�� ���")
							|| orgType.equalsIgnoreCase("������ ����������� ����������") || orgType.equalsIgnoreCase("��������� �������������� ����������")
							|| orgType.equalsIgnoreCase("������ ��������� ����������") || orgType.equalsIgnoreCase("��������� ���������� ����������")
							|| orgType.equalsIgnoreCase("������ ������������ ����������") || orgType.equalsIgnoreCase("������ ���������� ����������")
							|| orgType.equalsIgnoreCase("�/�") || orgType.equalsIgnoreCase("���")
							|| orgType.equalsIgnoreCase("������� ������������� ������������� �������") || orgType.equalsIgnoreCase("������� ������������� ������������ �������")
							|| orgType.equalsIgnoreCase("���������� ������������� ������������� �������") || orgType.equalsIgnoreCase("�������.��������� ��������������� ����������")){		
							
						thisDirectory = hm.directoryCreator (PublicPath, thisPath);				   		// Create the exact directory of each Gemh File
						thisPath = "/PUBLIC" + thisPath;											   	// /PUBLIC/AFM/GEMH/GEMH_TYPE	
					}
					// Check if company is PUBLIC/PUBLIC.PLE
					else if (orgType.equalsIgnoreCase("������ ������� �������� �������") || orgType.equalsIgnoreCase("���� ���� ������������") 
							|| orgType.equalsIgnoreCase("���� ���� �� ������������")){		
							
						thisDirectory = hm.directoryCreator (PublicPlePath, thisPath);				   	// Create the exact directory of each Gemh File
						thisPath = "/PUBLIC/PUBLIC.PLE" + thisPath;										// /PUBLIC/PUBLIC.PLE/AFM/GEMH/GEMH_TYPE	
					}
					// Check if company is PUBLIC/PUBLIC.MUNIC
					else if (orgType.equalsIgnoreCase("�������� ���������� ����������") || orgType.equalsIgnoreCase("������ �������� ����������")
							|| orgType.equalsIgnoreCase("�������� �������������� ����������") || orgType.equalsIgnoreCase("�� ���")
							|| orgType.equalsIgnoreCase("������ ����������� ����������") || orgType.equalsIgnoreCase("��������� �������������� ����������")
							|| orgType.equalsIgnoreCase("������ ��������� ����������") || orgType.equalsIgnoreCase("��������� ���������� ����������")
							|| orgType.equalsIgnoreCase("������ ���������� ����������") || orgType.equalsIgnoreCase("������ ������������ ����������")){		
							
						thisDirectory = hm.directoryCreator (PublicMunicPath, thisPath);				 // Create the exact directory of each Gemh File
						thisPath = "/PUBLIC/PUBLIC.MUNIC" + thisPath;									 // /PUBLIC/PUBLIC.MUNIC/AFM/GEMH/GEMH_TYPE	
					}
					// Check if company is LAWERS
					else if (orgType.equalsIgnoreCase("������ ������������� ������� ���������")){		
							
						thisDirectory = hm.directoryCreator (LawersPath, thisPath);				   		// Create the exact directory of each Gemh File
						thisPath = "/LAWERS" + thisPath;											   	// /LAWERS/AFM/GEMH/GEMH_TYPE	
					}
					// Check if company is NOTARIES
					else if (orgType.equalsIgnoreCase("������ �������� ������� ���������������")
							|| orgType.equalsIgnoreCase("������ ������������� ������� ���������������")){		
							
						thisDirectory = hm.directoryCreator (NotariesPath, thisPath);				   	// Create the exact directory of each Gemh File
						thisPath = "/NOTARIES" + thisPath;											   	// /NOTARIES/AFM/GEMH/GEMH_TYPE	
					}
					// Check if company is BAILIFF
					else if (orgType.equalsIgnoreCase("������ ����� ����� ���������� ����������")
							|| orgType.equalsIgnoreCase("������ ������������� ������� ���������� ����������")){		
							
						thisDirectory = hm.directoryCreator (BailiffPath, thisPath);				   	// Create the exact directory of each Gemh File
						thisPath = "/BAILIFF" + thisPath;											   	// /BAILIFF/AFM/GEMH/GEMH_TYPE	
					}
					// Check if ASSOCIATION
					else if (orgType.equalsIgnoreCase("��������") || orgType.equalsIgnoreCase("��������")
							|| orgType.equalsIgnoreCase("���� ����� �������� �� ������������") 
							|| orgType.equalsIgnoreCase("���� ����� �������� ������������")){
							
						thisDirectory = hm.directoryCreator (AssociationPath, thisPath);				// Create the exact directory of each Gemh File
						thisPath = "/ASSOCIATION" + thisPath;										   	// /ASSOCIATION/AFM/GEMH/GEMH_TYPE	
					}
					// Check if SHIP
					else if (orgType.equalsIgnoreCase("������� ������� �.959/79") || orgType.equalsIgnoreCase("������ ������� �������")
							|| orgType.equalsIgnoreCase("��������������") || orgType.equalsIgnoreCase("���� ������ ����� ������ ������ �3182/03")
							|| orgType.equalsIgnoreCase("AN 378/68 ��� � 27/75 ����������� �����")){
							
						thisDirectory = hm.directoryCreator (ShipPath, thisPath);				   		// Create the exact directory of each Gemh File
						thisPath = "/SHIP" + thisPath;										   			// /SHIP/AFM/GEMH/GEMH_TYPE	
					}
					// Check if company is FOUNDATION
					else if (orgType.equalsIgnoreCase("������")){		
							
						thisDirectory = hm.directoryCreator (FoundationPath, thisPath);				   // Create the exact directory of each Gemh File
						thisPath = "/FOUNDATION" + thisPath;										   // /FOUNDATION/AFM/GEMH/GEMH_TYPE	
					}
					// Check if company is INTERNATIONAL
					else if (orgType.equalsIgnoreCase("������� ����������")){		
							
						thisDirectory = hm.directoryCreator (InternationalPath, thisPath);				// Create the exact directory of each Gemh File
						thisPath = "/INTERNATIONAL" + thisPath;											// /INTERNATIONAL/AFM/GEMH/GEMH_TYPE	
					}
					// Check if company is CHARITY
					else if (orgType.equalsIgnoreCase("�������� ������")){		
							
						thisDirectory = hm.directoryCreator (CharityPath, thisPath);				   	// Create the exact directory of each Gemh File
						thisPath = "/CHARITY" + thisPath;											   	// /CHARITY/AFM/GEMH/GEMH_TYPE	
					}
					// Check if company is POLITICAL
					else if (orgType.equalsIgnoreCase("�������� ������� ��������� ����������")){		
							
						thisDirectory = hm.directoryCreator (PoliticalPath, thisPath);				 	// Create the exact directory of each Gemh File
						thisPath = "/POLITICAL" + thisPath;											 	// /POLITICAL/AFM/GEMH/GEMH_TYPE	
					}
					else{
						
						thisDirectory = hm.directoryCreator (UnPath, thisPath);	
						thisPath = "/OTHER" + thisPath;													// /OTHER/AFM/GEMH/GEMH_TYPE
						hm.writeMetadata(ExportPath+"/notMatchedOrgTypes" +hm.getPreviusDate(), orgType);
						
					}
						
					downloadPath = thisDirectory.getPath().toString();
						
					String fileName = null;														// fileName in greeklish				
					String fileNameGr = null;													// fileName in greek
					String fileType = null;
					fileLink = null;															// file file download link
					
					//Recommendation_Data
					if (gemhType.equalsIgnoreCase("Recommendation_Data")){
						
						fileName = fields[4].replaceAll("-", "_") + "_" 						// gemhStartDate
								+ fields[5].replaceAll("[\"]","") + "_"							// decisionID
								+ fields[0].replaceAll("[\"]","");								// AFM.fileType
					
						fileLink = fields[6].replaceAll("[\"]","");								// file file download link
						
						if (!fileLink.contains("https")){
							continue;
						}
					}
					//Announcement_Archives
					else if(gemhType.equalsIgnoreCase("Announcement_Archives")){
						
						String greekSubject = fields[4];												// greek subject
						String greeklishSubject = hm.convertToGreeklish(fields[4]);						// convert to greeklish
						
						fileName = greeklishSubject.replaceAll("-", "_").replaceAll("\\/", "_") + "_" 	// subject
								+ fields[5].replaceAll("[\"]","") + "_"									// decisionDate
								+ fields[6].replaceAll("[\"]","") + "_"									// decisionId
								+ fields[0].replaceAll("[\"]","");										// AFM.fileType
					
						fileNameGr = greekSubject.replaceAll("-", "_").replaceAll("\\/", "_") + "_" 	// subject
								+ fields[5].replaceAll("[\"]","") + "_"									// decisionDate
								+ fields[6].replaceAll("[\"]","") + "_"									// decisionId
								+ fields[0].replaceAll("[\"]","");										// AFM.fileType
					
						fileLink = fields[7].replaceAll("[\"]","");										// file file download link
						
						if (!fileLink.contains("https")){
							continue;
						}
					}
					//Files_Making_Bodies
					else if(gemhType.equalsIgnoreCase("Records_of_Decisions_organs")){
						
						String greekSubject = fields[4];										// greek subject
						String greeklishSubject = hm.convertToGreeklish(fields[4]);				// convert to greeklish
												
						fileName = greeklishSubject.replaceAll("-", "_") + "_" 					// bodyType
								+ fields[5].replaceAll("[\"]","") + "_"							// decisionCode
								+ fields[6].replaceAll("[\"]","") + "_"							// decisionDate
								+ fields[0].replaceAll("[\"]","");								// AFM.fileType
						
						fileNameGr = greekSubject.replaceAll("-", "_") + "_" 					// subject
								+ fields[5].replaceAll("[\"]","") + "_"							// decisionDate
								+ fields[6].replaceAll("[\"]","") + "_"							// decisionId
								+ fields[0].replaceAll("[\"]","");								// AFM.fileType
					
						fileLink = fields[7].replaceAll("[\"]","");								// file file download link
						
						if (!fileLink.contains("https")){
							continue;
						}
					}
					//Others
					else if(gemhType.equalsIgnoreCase("Other_Files")){
						
						String greekSubject = fields[4];												// greek subject
						String greeklishSubject = hm.convertToGreeklish(fields[4]);						// convert to greeklish
						
						fileName = greeklishSubject.replaceAll("-", "_").replaceAll("\\/", "_") + "_" 	// subject
								+ fields[5].replaceAll("[\"]","") + "_"									// decisionDate
								+ fields[0].replaceAll("[\"]","");										// AFM.fileType
					
						fileNameGr = greekSubject.replaceAll("-", "_").replaceAll("\\/", "_") + "_" 	// subject
								+ fields[5].replaceAll("[\"]","") + "_"									// decisionDate
								+ fields[6].replaceAll("[\"]","") + "_"									// decisionId
								+ fields[0].replaceAll("[\"]","");										// AFM.fileType
					
						fileLink = fields[6].replaceAll("[\"]","");										// file file download link
						
						if (!fileLink.contains("https")){
							continue;
						}	
					}
					//Records_of_Decisions_Legality_Control_Authorities
					else if(gemhType.equalsIgnoreCase("Records_of_Decisions_Legality_Control_Authorities")){
						
						String greekSubject = fields[4];												// greek subject
						String greeklishSubject = hm.convertToGreeklish(fields[4]);						// convert to greeklish
						
						fileName = greeklishSubject.replaceAll("-", "_").replaceAll("\\/", "_") + "_" 	// subject
								+ fields[5].replaceAll("[\"]","") + "_"									// decisionDate
								+ fields[0].replaceAll("[\"]","");										// AFM.fileType
					
						fileNameGr = greekSubject.replaceAll("-", "_").replaceAll("\\/", "_") + "_" 	// subject
								+ fields[5].replaceAll("[\"]","") + "_"									// decisionDate
								+ fields[6].replaceAll("[\"]","") + "_"									// decisionId
								+ fields[0].replaceAll("[\"]","");										// AFM.fileType
					
						fileLink = fields[6].replaceAll("[\"]","");										// file file download link
						
						if (!fileLink.contains("https")){
							continue;
						}
							
					}
						
					/** Check if file already exists **/
					//boolean exists = hm.fileExists(downloadPath, fileName);
					boolean exists = hm.fileExists(thisPath, fileName, fileNameGr);
					
					if (exists){
								
						System.out.println("\nFile " + fileName + " already exist!\n");
						continue;
							
					}
					else if (!exists) {					
							
						System.out.println("downloadPath: " + downloadPath);
						
						try{
							
							boolean downloaded = hm.fileDownload(fileLink, downloadPath, vatId);				// returns true if file is downloaded and false -after 60 secs- if it is not
							
							// if file is downloaded we rename it
							if (downloaded){
										
								File fileFile = hm.getTheNewestFile(downloadPath);								// Get the last file File in directory
								
								System.out.println("The newest file name is : " +fileFile.getName());
								
								fileType = hm.getFileType(fileFile);											// Get the newest file .ext
								
								System.out.println("The newest file type is : " +fileType);
								
								fileName = fileName +"."+ fileType;
									
								hm.fileRenamer(fileFile, fileName, downloadPath);								// Rename the last file file
										
								hm.writeMetadata(ExportPath+"/downloadArchivePaths_" +hm.getPreviusDate(), downloadPath+"/"+fileName);
								
								boolean uploaded = false;
								
								/** CompanyDb Connection **/
								CompanyDb cdb=new CompanyDb("jdbc:mysql://83.212.86.155/DbCompany?useUnicode=true&characterEncoding=UTF-8"
															,"gemh"
															,"gemh@69");
						    	
								/** Dropbox uploading **/
								String publicLink = null;
										
								try {
									
									//publicLink = dba.upload(downloadPath+"\\"+fileName,"/company"+thisPath+"/"+fileName,true);
									publicLink = dba.upload(downloadPath+"/"+fileName,"/company"+thisPath+"/"+fileName,true);
									System.out.println(publicLink);
									uploaded = true;
								} catch (DbxException e1) {
									uploaded = false;
									hm.writeMetadata("notUploaded", data);
									e1.printStackTrace();
								}
								
								/** add new file at companyDb **/
								if(uploaded){
									
									List<String> detailsList = hm.getDetails(thisPath);
									
									int CdbOrgId = cdb.insertOrgType(detailsList.get(0));
							    	int CdbVatId = cdb.insertOrgVat(CdbOrgId, detailsList.get(1));
							    	int CdbGemhId = cdb.insertGemhs(CdbVatId, detailsList.get(2));
							    	int CdbCategoryId = cdb.insertCategory(detailsList.get(3));
							    	int gemhFiles = cdb.insertGemhFiles(CdbCategoryId, CdbGemhId, fileName, publicLink);
							    	
							    	hm.writeMetadata(ExportPath+"/uploadedArchivePaths_" +hm.getPreviusDate(), "App/LB/company"+thisPath+"/"+fileName +","+publicLink);
									
							    }
								
								cdb.close();
								
							}
							// else write the not downloaded file data to a txt
							else {
								hm.writeMetadata("notDownloaded", data);
							}
							
						}	
						catch (IOException e) {
							e.printStackTrace();
						}
						
					}
				}
		
			} catch (FileNotFoundException e) {
			e.printStackTrace();
			}
			
			System.out.println("--------------------- Finished for "+hm.getPreviusDate()+" ------------------------");
			
			//hm.sleepMode(2);									// Enter in sleep mode until next next day at 02:01:00
        
		}
	}	
}